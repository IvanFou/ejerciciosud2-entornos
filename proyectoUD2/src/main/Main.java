package main;

import java.util.Scanner;

public class Main {
	static Scanner escaner = new Scanner(System.in);
	public static void main(String[] args) {
		// TODO Auto-generated method stub		
		
		System.out.println("Introduce varias palabras");
		
		String palabras = escaner.nextLine();
		
		palabraLarga(palabras);

	}
	
	private static String palabraLarga(String palabras) {
		if(!palabras.contains(" ")) {
			return palabras;
		}
		String palabritaLarga = palabras.substring(0, palabras.indexOf(' '));
		String palabraEncontrada;
		
		int espacioInicio = palabras.indexOf(' ');
		
	    for(int i = espacioInicio + 1; i < palabras.length(); i++){
            
            if(palabras.charAt(i) == ' ' || i == (palabras.length() - 1)){
            
                palabraEncontrada = palabras.substring(espacioInicio + 1, i);
       
                if(palabraEncontrada.length() > palabritaLarga.length()){
                    palabritaLarga = palabraEncontrada; 
                }
                espacioInicio = i;
            }


        }
        return palabritaLarga;
	}

}
